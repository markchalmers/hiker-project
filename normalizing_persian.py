# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import pandas

df = pandas.read_excel('C:\\Users\\Mark\\Desktop\\GLMM\\clean_persian_text.xlsx')
#this did not work to remove the quotes
df['clean_tweets'] = df['clean_tweets'].str.replace('”', '')

df['clean_tweets'] = df['clean_tweets'].str.replace('“', '')

df['clean_tweets'] = df['clean_tweets'].str.replace('«', '')

df['clean_tweets'] = df['clean_tweets'].str.replace('»', '')
 
from hazm import *

normalizer = Normalizer()

df['normalized_tweets'] = df['clean_tweets'].apply(normalizer.normalize)

stemmer = Stemmer()

df['stemmed_normalized_tweets'] = df['normalized_tweets'].apply(stemmer.stem)

df['stemmed_tweets'] = df['clean_tweets'].apply(stemmer.stem)

df['normalized_stemmed_tweets'] = df['stemmed_tweets'].apply(normalizer.normalize)

writer = pandas.ExcelWriter('clean_persian.xlsx', engine='xlsxwriter')

# Convert the dataframe to an XlsxWriter Excel object.
df.to_excel(writer, sheet_name='Sheet1')

# Close the Pandas Excel writer and output the Excel file.
writer.save()