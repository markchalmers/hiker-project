library(qdap)
library(tm)
library(readxl)
library(xlsx)
#library(textcat)
library(cld2)
library(dplyr)
library(smappR)
library(readr)

setwd("C:/Users/Mark/Desktop/GLMM")
#read excel keeps the persian characters 
persian <- read_excel("persian_tweets.xlsx")

persiantweets <- persian$tweets

persiantweets_source <- VectorSource(persiantweets)

persiantweets_corpus <- VCorpus(persiantweets_source)

toSpace = content_transformer( function(x, pattern) gsub(pattern," ",x) )

punct <- '[]\\?!\"\'#$%&(){}+*/:;,._`|~\\[<=>@\\^-]«»'

#Removing "RT @user:", punctuations, links from the tweets. 
clean_persian_corpus <- function(corpus){
  corpus <- tm_map(corpus, toSpace, "http[^[:space:]]*")
  corpus <- tm_map(corpus, removePunctuation)
  corpus <- tm_map(corpus, removeNumbers)
  corpus <- tm_map(corpus, content_transformer(tolower))
  corpus <- tm_map(corpus, toSpace, "[a-z]")
  corpus <- tm_map(corpus, toSpace, punct)
  corpus <- tm_map(corpus, stripWhitespace)
  return(corpus)
}

clean_persian <- clean_persian_corpus(persiantweets_corpus) 

#define the function
outpersian <- sapply(clean_persian, function(x){x$content})
#run it
outpersian

#turn the output into a dataframe
outpersian2 <- as.data.frame(outpersian, stringsAsFactors = FALSE)

#define just the text column
persian_text <- outpersian2[,1]

head(persian_text)

clean_persian_text <- data.frame(clean_tweets = persian_text, lang = persian$lang, created_at = persian$created_at)

head(clean_persian_text)

write.xlsx(x = clean_persian_text, file = "clean_persian_text2.xlsx",
           sheetName = "clean persian tweets", row.names = FALSE) 
